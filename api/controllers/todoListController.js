'use strict';
let mongoose = require('mongoose'),
  Task = mongoose.model('Tasks');

function extractData(mongoEntity) {
  return {
    id: mongoEntity._id,
    name: mongoEntity.name,
    status: mongoEntity.status,
    date_created: mongoEntity.date_created
  };
}

exports.listTasks = function(req, res) {
  Task.find({}, function(err, tasks) {
    if (err) {
      res.send(err);
    }

    let entities = [];

    for (const t of tasks) {
      entities.push(extractData(t));
    }

    res.json(entities);
  });
};

exports.createTask = function(req, res) {
  let newTask = new Task(req.body);
  newTask.save(function(err, task) {
    if (err) {
      res.send(err);
    }

    res.json(extractData(task));
  });
};

exports.getTaskById = function(req, res) {
  Task.findById(req.params.taskId, function(err, task) {
    if (err) {
      res.send(err);
    }

    res.json(extractData(task));
  });
};

exports.updateTask = function(req, res) {
  Task.findOneAndUpdate({_id: req.params.taskId}, req.body, {new: true}, function(err, task) {
    if (err) {
      res.send(err);
    }

    res.json(extractData(task));
  });
};

exports.deleteTask = function(req, res) {
  Task.remove({
    _id: req.params.taskId
  }, function(err, task) {
    if (err) {
      res.send(err);
    }

    res.json({ message: 'Task successfully deleted' });
  });
};
