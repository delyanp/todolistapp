import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class TasksService {

  constructor(private http: Http) { }

  getTasks() {
    return this.http.get('http://localhost:3000/tasks');
  }

  completeTask(taskId: string) {
    return this.http.put(`http://localhost:3000/tasks/${taskId}`, {
      status: 'completed'
    });
  }
}
