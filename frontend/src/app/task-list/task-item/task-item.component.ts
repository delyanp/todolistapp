import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Task } from '../../task.model';
import { TasksService } from '../../tasks.service';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent {
  @Input() task: Task;
  @Output() taskUpdate = new EventEmitter();

  constructor(private tasksService: TasksService) { }

  onTaskComplete() {
    this.tasksService.completeTask(this.task.id).subscribe((res) => {
      this.taskUpdate.emit(res.json());
    });
  }
}
