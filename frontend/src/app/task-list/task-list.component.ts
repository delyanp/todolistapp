import { Component, OnInit } from '@angular/core';
import { TasksService } from '../tasks.service';
import { Task } from '../task.model';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
  tasks: Task[];

  constructor(private tasksService: TasksService) { }

  ngOnInit() {
    this.getTasks();

    this.tasksService.completeTask('5c33b065150bfa459210dd8f').subscribe(() => {
      console.log('hellog');
    });
  }

  onTaskUpdate(): void {
    this.getTasks();
  }

  private getTasks(): void {
    this.tasksService.getTasks().subscribe((res) => {
      this.tasks = res.json();

      this.tasks.sort((a, b) => {
        return a.status.includes('completed') ? 1 : -1;
      });
    });
  }
}
