'use strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let TaskSchema = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the task'
  },
  status: {
    type: [{
      type: String,
      enum: ['pending', 'completed']
    }],
    default: ['pending']
  },
  date_created: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model('Tasks', TaskSchema);